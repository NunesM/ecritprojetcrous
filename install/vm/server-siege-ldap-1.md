```bash
# Installer openldap
# lien tuto: https://openclassrooms.com/fr/courses/1733551-gerez-votre-serveur-linux-et-ses-services/5236041-gerez-votre-annuaire-ldap
sudo apt install -y slapd ldap-utils #password: projet-crous33
sudo dpkg-reconfigure slapd # choisir les options: no > crous.lan > crous > projet-crous33 > projet-crous33 > yes > yes

# Installer phpldapadmin
# Lien du tuto: https://kifarunix.com/install-phpldapadmin-on-debian-10-debian-11/
wget http://ftp.fr.debian.org/debian/pool/main/p/phpldapadmin/phpldapadmin_1.2.2-6.1~bpo9+1_all.deb
sudo apt install -y ./phpldapadmin_1.2.2-6.1~bpo9+1_all.deb

sudo cp /etc/phpldapadmin/config.php /etc/phpldapadmin/config_original.php
sudo vi /etc/phpldapadmin/config.php
# Modifier les lignes
# $servers->setValue('server','name','Crous LDAP Server');
# $servers->setValue('server','base',array('dc=crous,dc=lan'));
# $servers->setValue('login','bind_id','cn=admin,dc=crous,dc=lan');

sudo vi /etc/apache2/conf-available/phpldapadmin.conf
<<'content-/etc/apache2/conf-available/phpldapadmin.conf'
Alias /phpldapadmin /usr/share/phpldapadmin/htdocs

<Directory /usr/share/phpldapadmin/htdocs>
  <IfModule mod_authz_core.c>
    Require all granted
  </IfModule>
</Directory>
content-/etc/apache2/conf-available/phpldapadmin.conf

sudo apt-get install php-ldap php-xml
# Remplecer le contenu du fichier par celui de file/functions.php, du au problème https://stackoverflow.com/questions/50698477/cant-create-new-entry-phpldapadmin
sudo cp /usr/share/phpldapadmin/lib/functions.php /usr/share/phpldapadmin/lib/functions_original.php
sudo vi /usr/share/phpldapadmin/lib/functions.php

sudo systemctl restart apache2

# Importer les premiers données
sudo systemctl stop slapd
slapadd -c -b dc=crous,dc=lan -F /etc/ldap/slapd.d -l first_backup.ldif
sudo systemctl restart slapd

# Configure le cron de backup
sudo apt-get install -y borgbackup
sudo useradd borg --create-home --home-dir /home/borg/ --shell /bin/bash
sudo passwd borg
sudo usermod -aG sudo borg
su borg -
ssh-keygen
mkdir /home/borg/backup/
mkdir /home/borg/backup/ldap
cat > /home/borg/backup/ldap/passphrase-server-siege-backup-1 # mettre le mot de passe par défaut
chmod 700 /home/borg/backup/ldap/passphrase-server-siege-backup-1
mkdir /home/borg/cron
vi /home/borg/cron/server-siege-backup-1-ldap.sh
<<'content-/etc/cron.daily/server-siege-backup-1-ldap.sh'
#!/bin/sh
# Script de sauvegarde
# Envoie les sauvegardes du ldap sur un serveur distant, via le programme Borg
# Les sauvegardes sont chiffrées

set -e
BACKUP_DATE=`date +%Y-%m-%d`
LOG_PATH=/home/borg/backup.log
export BORG_PASSPHRASE="`cat /home/borg/backup/ldap/passphrase-server-siege-backup-1`"
BORG_REPOSITORY=borg@server-siege-backup-1:/home/borg/backup/server-siege-ldap-1
BORG_ARCHIVE=${BORG_REPOSITORY}::${BACKUP_DATE}

systemctl stop slapd
slapcat -b dc=crous,dc=lan -l /home/borg/backup/ldap/${BACKUP_DATE}_backup-ldap-data.ldif
tar -cf /home/borg/backup/ldap/${BACKUP_DATE}_backup-ldap-conf.tar /etc/ldap
systemctl start slapd

borg create \
	-v --stats --compression lzma,9 \
	$BORG_ARCHIVE \
	/home/borg/backup/ldap/${BACKUP_DATE}_backup-ldap.ldif /home/borg/backup/ldap/${BACKUP_DATE}_backup-ldap.tar \
	>> ${LOG_PATH} 2>&1
content-/home/borg/cron/server-siege-backup-1-ldap.sh
sudo crontab -e # besoin des droits sudo pour bien executer le script
# Ajouter à la fin du fichier la ligne: "00  01  *   *   * bash /home/borg/cron/server-siege-backup-1-ldap"
# Le script va s'éxecute tout les jours à 1h 
```

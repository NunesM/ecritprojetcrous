```bash
# Install prometheus and node_exporter
# link to tuto: https://computingforgeeks.com/how-to-install-prometheus-and-node-exporter-on-debian/
sudo apt-get install -y prometheus
sudo cat << EOF >> /etc/prometheus/prometheus.yml
  - job_name: 'node_exporter'
    static_configs:
     - targets: ['localhost:9100']
EOF
sudo systemctl restart prometheus

# Install grafana
sudo apt-get install -y apt-transport-https
sudo apt-get install -y software-properties-common wget
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
echo "deb https://packages.grafana.com/oss/deb beta main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
sudo apt-get update
sudo apt-get install grafana
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable grafana-server
sudo /bin/systemctl start grafana-server

# Connect prometheus to grafana
# doc: https://prometheus.io/docs/visualization/grafana/#using
# Data Sources > Prometheus > Dashboards > Import the Prometheus 2.0 Stats dashboard

# Install dnsmasq
sudo vi /etc/network/interfaces
<< 'add-content-/etc/network/interfaces'
auto enp0s3
iface enp0s3 inet static
 address 10.0.50.1/24
/etc/network/interfaces

sudo apt-get update
sudo apt-get install -y dnsmasq
sudo cp /etc/dnsmasq.conf /etc/dnsmasq_original.conf
sudo vi /etc/dnsmasq.conf
<< 'content-/etc/dnsmasq.conf'
#### DNS ####
domain-needed
bogus-priv
# Ficher des forwarders
strict-order
expand-hosts
domain=crous.lan
# LOG DNS
log-queries
no-negcache
interface=enp0s3

#### DHCP ####
log-dhcp
# Options GLOBALES
dhcp-option=option:dns-server,10.0.50.1
dhcp-option=option:domain-name,crous.lan
dhcp-range=vlan-admin-pc,10.0.10.1,10.0.10.253,255.255.255.0,24h
dhcp-option=vlan-admin-pc,option:router,10.0.10.254
dhcp-option=vlan-admin-pc,option:netmask,255.255.255.0
dhcp-range=vlan-admin-phone,10.0.20.1,10.0.20.253,255.255.255.0,24h
dhcp-option=vlan-admin-phone,option:router,10.0.20.254
dhcp-option=vlan-admin-phone,option:netmask,255.255.255.0
dhcp-range=vlan-admin-printer,10.0.30.1,10.0.30.253,255.255.255.0,24h
dhcp-option=vlan-admin-printer,option:router,10.0.30.254
dhcp-option=vlan-admin-printer,option:netmask,255.255.255.0
dhcp-range=vlan-admin-wifi,10.0.40.1,10.0.40.253,255.255.255.0,24h
dhcp-option=vlan-admin-wifi,option:router,10.0.40.254
dhcp-option=vlan-admin-wifi,option:netmask,255.255.255.0
dhcp-range=vlan-admin-server,10.0.50.5,10.0.50.253,255.255.255.0,24h
dhcp-option=vlan-admin-server,option:router,10.0.50.254
dhcp-option=vlan-admin-server,option:netmask,255.255.255.0
dhcp-range=vlan-admin-server,10.0.60.1,10.0.60.253,255.255.255.0,24h
dhcp-option=vlan-admin-server,option:router,10.0.60.254
dhcp-option=vlan-admin-server,option:netmask,255.255.255.0
dhcp-range=vlan-admin-server,10.0.70.1,10.0.70.253,255.255.255.0,24h
dhcp-option=vlan-admin-server,option:router,10.0.70.254
dhcp-option=vlan-admin-server,option:netmask,255.255.255.0
dhcp-range=vlan-admin-server,10.0.99.1,10.0.99.253,255.255.255.0,24h
dhcp-option=vlan-admin-server,option:router,10.0.99.254
dhcp-option=vlan-admin-server,option:netmask,255.255.255.0
content-/etc/dnsmasq.conf

sudo vi /etc/hosts
<< 'add-content-/etc/hosts'
10.0.50.1 server-siege-dhcp-dns-1
10.0.50.2 server-siege-ldap-1
10.0.50.3 server-siege-backup-1
10.0.50.4 crous-logement.fr server-siege-web-1
add-content-/etc/hosts

sudo vi /etc/resolv.conf
<< 'add-content-/etc/resolv.conf'
nameserver 8.8.8.8
nameserver 8.8.4.4
add-content-/etc/resolv.conf

sudo systemctl enable dnsmasq
sudo systemctl restart dnsmasq

sudo apt install -y firewalld
sudo firewall-cmd --add-service=dhcp --permanent
sudo firewall-cmd --add-service=dns --permanent
sudo firewall-cmd --reload

```bash
# Préparer le serveur de backup pour acceuillir les backups
sudo apt-get install -y openssh-server borgbackup
useradd borg --create-home --home-dir /home/borg/ --shell /bin/bash
passwd borg
su borg -
mkdir /home/borg/backup/

# Configurer pour acceuillir les backups provenant de server-siege-ldap-1
borg init --encryption=repokey /home/borg/backup/server-siege-ldap-1
vi /home/borg/.ssh/authorized_keys # mettre la clé publique de l'utilisateur root de server-siege-ldap-1
```

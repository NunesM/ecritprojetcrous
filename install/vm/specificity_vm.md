# Specificity_vm
## Specs
- OS: debian
- RAM: 1024 MB
- stockage: 8 GB
- domain: crous.lan

## Ajouter le user defaut dans le groupe sudo
```bash
su -
usermod -aG sudo defaut
```

## Mettre l’interface en mode dhcp
```bash
vi /etc/network/interfaces
<< 'add-content-/etc/resolv.conf'
auto enp0s3
inet enp0s3 dhcp
add-content-/etc/resolv.conf
```

## Définir les serveurs DNS
```bash
sudo vi /etc/resolv.conf
<< 'add-content-/etc/resolv.conf'
nameserver 10.0.50.1 # server-siege-dhcp-dns-1
nameserver 8.8.8.8
add-content-/etc/resolv.conf
```

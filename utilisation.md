# Utilisation

## Prometheus
- http://localhost:9090

## Grafana
- http://localhost:3000/
- user et mot de passe par défaut: admin et admin

## OpenLDAP
```bash
sudo slapcat -b dc=crous,dc=lan -l mon_backup.ldif # sauvegarder les données
sudo tar -cvf ma_conf_g_ldap.tar /etc/ldap # sauvegarder la configuration

# Restaurer les données à partir d'un fichier
sudo systemctl stop slapd
slapadd -c -b dc=crous,dc=lan -F /etc/ldap/slapd.d -l mon_backup.ldif
sudo systemctl restart slapd

# Ajouter des données à partir d'un fichier
vi structure.ldif
<< 'content-structure.ldif'
dn: ou=Users,dc=crous,dc=lan
objectclass: organizationalUnit
ou: Users

dn: ou=Sites,dc=crous,dc=lan
objectclass: organizationalUnit
ou: Sites 

dn: ou=Residents,ou=Users,dc=crous,dc=lan
objectclass: organizationalUnit
ou: Residents
content-structure.ldif
sudo ldapadd -x -W -D "cn=admin,dc=crous,dc=lan" -H ldap://localhost -f structure.ldif
```

## PhpLDAPadmin
- localhost/phpldapadmin

## Apache
- crous-logement.fr
